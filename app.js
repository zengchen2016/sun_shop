import promiseFromWXCallback from './lib/promiseFromWXCallback';
import request from './lib/request';
import resource from './lib/resource';

const login = promiseFromWXCallback(wx.login);
const getUserInfo = promiseFromWXCallback(wx.getUserInfo);
const wxRequest = promiseFromWXCallback(wx.request);
// app.js

App({
  data : {
    token : null,
  },
  globalData: {
    appId:'wx3bce24d137585996',
    appSecret:'09ca5f5b8610ea7d79f5870499b368c0'
  },
});
