var imgServerUrl = 'http://118.31.11.178:8080/img/';
var bannerData = [{
  id: 0,
  title: "轮播图1",
  shop_id: "1",
  image_src: imgServerUrl + "banner01.jpg",
  order: "1",
  publish_time: "2016-11-18 12:36:21",
  end_time: "2016-11-18 12:36:21",
},
{
  id: 1,
  title: "轮播图2",
  shop_id: "1",
  image_src: imgServerUrl + "banner02.jpg",
  order: "2",
  publish_time: "2016-11-18 12:36:21",
  end_time: "2016-11-18 12:36:21",
}
];
var categoryData = [
  {
    id: 1,
    title: "春笋",
    image_src: imgServerUrl + "spring.jpg",
    sort: 1,
    type: 1,
    link_id: 1,
    link_url: "",
    created_time: "2016-10-08 13:58:16",
    publish_time: "2016-10-08 13:58:20",
    end_time: "2016-10-09 00:00:00",
    status: "published",
    admin_id: "ateddy",
    deleted_at: null
  },
  {
    id: 2,
    title: "冬笋",
    image_src: imgServerUrl + "winter.jpg",
    sort: 2,
    type: 3,
    link_id: 0,
    link_url: "https://www.baidu.com/",
    created_time: "2016-10-08 15:54:16",
    publish_time: "2016-10-08 15:54:19",
    end_time: "2016-10-08 19:53:56",
    status: "published",
    admin_id: "ateddy",
    deleted_at: null
  },
  {
    id: 3,
    title: "笋干",
    image_src: imgServerUrl + "summer.jpg",
    sort: 3,
    type: 2,
    link_id: 6,
    link_url: "",
    created_time: "2016-10-08 11:17:28",
    publish_time: "2016-10-08 11:17:53",
    end_time: "2016-10-08 11:17:53",
    status: "published",
    admin_id: "ateddy",
    deleted_at: null
  }
];

var titleData = {
    indexProductList:"热卖商品",
}

var productData = [
  {
    id: 1,
    shop_id: 1,
    goods_desc: "湖南特产竹笋干 笋尖干货500g 野生嫩冬笋干新货",
    goods_price: 19.9,
    goods_id: 1,
    status: "sold",
    sort: 1,
    name: "湖南特产竹笋干",
    image_url: imgServerUrl + "index01.jpg",
    is_sold_out:false,
  },
  {
    id: 1,
    shop_id: 1,
    goods_desc: "湖南特产竹笋干 笋尖干货500g 野生嫩冬笋干新货",
    goods_price: 19.9,
    goods_id: 1,
    status: "sold",
    sort: 1,
    name: "湖南特产竹笋干",
    image_url: imgServerUrl + "index01.jpg",
    is_sold_out: true,
  }, {
    id: 1,
    shop_id: 1,
    goods_desc: "湖南特产竹笋干 笋尖干货500g 野生嫩冬笋干新货",
    goods_price: 19.9,
    goods_id: 1,
    status: "sold",
    sort: 1,
    name: "湖南特产竹笋干",
    image_url: imgServerUrl + "index01.jpg",
    is_sold_out: false,
  }
  ]

module.exports = {
  bannerData: bannerData,
  categoryData: categoryData,
  titleData: titleData,
  productData: productData,
}